package javatest.demo;

import javatest.demo.service.ListProcess;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {
    public String name;
    public int old;

    public DemoApplication() {

    }

    public DemoApplication(String name) {
        this.name = name;
    }

    public void setOld(int old) {
        String s = (old != 0) ? "Setting ..." : "Old not set";
        System.out.println(s);
        // vi du ve switch case
        switch (old) {
            case 27:
                System.out.println("Ban la nguoi truong thanh");
                break;
            case 40:
                System.out.println("ban qua gia");
                break;
            default:
                System.out.println("ban con qua tre");
        }

        this.old = old;
    }

    public void getInformation() {
        System.out.println("Name:" + name);
        System.out.println("Old:" + old);
    }

    public static void main(String[] args) {
        DemoApplication sv = new DemoApplication("MAC DUY HAI");
        sv.setOld(27);
        sv.getInformation();
        String[] cars = {"Honda", "BMW", "Ford"};
        //Sap xep mang tang dan
        Arrays.sort(cars);
        // Khoi tao mang rong
        String[] friends;
        friends = new String[]{"haimd", "simon"};

        for (String car : cars) {
            System.out.println(car);
        }
        for (String fd : friends) {
            System.out.println(fd);
        }
        // Demo List
        System.out.println("Demo type List ");
        ListProcess list = new ListProcess();
        String[] animals = {"Dog", "Cat", "Mouse"};
        list.setList(animals);
        list.getList();
//        SpringApplication.run(DemoApplication.class, args);
    }

}
