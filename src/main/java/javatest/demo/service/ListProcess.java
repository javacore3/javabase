package javatest.demo.service;


import java.util.ArrayList;
import java.util.List;

public class ListProcess {
    List<String> lists = new ArrayList<String>();
//    lists.add(0,"Dog")

    public ListProcess() {

    }

    public void setList(String[] animals) {
        for (String animal : animals) {
            lists.add(animal);
        }
    }

    public void getList() {
        for (String animal : lists) {
            System.out.println(animal);
        }
    }


}
